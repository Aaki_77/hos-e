package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.DoctorLoginDao;
import com.hollongtree.ehospital.entity.DoctorLogin;

@Service
public class DoctorLoginService {

	@Autowired
	DoctorLoginDao doctorLoginDao;
	
	/**
	 * Method for saving the doctor_login details to the Database
	 * @param doctorLogin
	 * @return
	 */
	public DoctorLogin addDoctorLogin(DoctorLogin doctorLogin) {
		
		DoctorLogin dl=doctorLoginDao.save(doctorLogin);
		return dl;
		
	}

	/**
	 * Method for updating the doctor_login details
	 * @param doctorLogin
	 * @param doctorLoginId 
	 * @return
	 */
	public DoctorLogin updateDoctorLogin(DoctorLogin doctorLogin, int doctorLoginId) {
		
		DoctorLogin dl=doctorLoginDao.save(doctorLogin);
		return dl;
	}

}
