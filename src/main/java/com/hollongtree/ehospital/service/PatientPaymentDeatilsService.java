package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.PatientPaymentDetailsDao;
import com.hollongtree.ehospital.entity.PatientPaymentDetails;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class PatientPaymentDeatilsService {
	
	@Autowired
	PatientPaymentDetailsDao patientPaymentDetailsDao;

	/**
	 * Method for save the data to the db
	 * @param patientPaymentDetails
	 * @return
	 */
	public PatientPaymentDetails addPatientPaymentDetails(PatientPaymentDetails patientPaymentDetails) {
		
		PatientPaymentDetails ppd=patientPaymentDetailsDao.save(patientPaymentDetails);
		return ppd;
	}

	/**
	 * Method for Update the entity
	 * @param patientPaymentDeatils
	 * @return
	 * @throws EhospitalException 
	 */
	public PatientPaymentDetails updatePatientPaymentDetails(PatientPaymentDetails patientsAppointmentDetails,int patientsAppointmentDetailsId) throws EhospitalException {
		
		PatientPaymentDetails ppd=patientPaymentDetailsDao.findById(patientsAppointmentDetailsId).orElseThrow(()->new EhospitalException("patientsAppointmentDetailsId not found"));
		
		ppd.setDoctorDetailsId(patientsAppointmentDetails.getDoctorDetailsId());
		ppd.setPatientsId(patientsAppointmentDetails.getPatientsId());
		ppd.setPatientsName(patientsAppointmentDetails.getPatientsName());
		ppd.setPatientsSubcategoryId(patientsAppointmentDetails.getPatientsSubcategoryId());
		ppd.setPaymentAmount(patientsAppointmentDetails.getPaymentAmount());
		ppd.setPaymentId(patientsAppointmentDetails.getPaymentId());
		ppd.setPaymentStatus(patientsAppointmentDetails.getPaymentStatus());
		ppd.setRemarks(patientsAppointmentDetails.getRemarks());
		ppd.setValidity(patientsAppointmentDetails.getValidity());
		ppd.setValidityEndTime(patientsAppointmentDetails.getValidityEndTime());
		ppd.setValidityFromTime(patientsAppointmentDetails.getValidityFromTime());
		ppd.setUdf1(patientsAppointmentDetails.getUdf1());
		ppd.setUdf2(patientsAppointmentDetails.getUdf2());
		ppd.setUdf3(patientsAppointmentDetails.getUdf3());
		ppd.setUdf4(patientsAppointmentDetails.getUdf4());
		ppd.setUdf5(patientsAppointmentDetails.getUdf5());
		PatientPaymentDetails ppd1=patientPaymentDetailsDao.save(ppd);
		return ppd1;
	}

	/**
	 * Method for fetching the data using patientsname, if not found then throw a exception
	 * @param patientsName
	 * @return
	 * @throws EhospitalException
	 */
	public PatientPaymentDetails getPatientPaymentDetails(String patientsName) throws EhospitalException {
		PatientPaymentDetails ppd=patientPaymentDetailsDao.findByPatientsName(patientsName);
		if(ppd==null)
		{
			throw new EhospitalException("patient name not found");
		}
		return ppd;
	}

	public PatientPaymentDetails getPatientPaymentDetailsByPatientsId(int patientsId) throws EhospitalException {
		PatientPaymentDetails ppd=patientPaymentDetailsDao.findByPatientsId(patientsId);
		if(ppd==null)
		{
			throw new EhospitalException("patientsId not found");
		}
		return ppd;
	}

}
