package com.hollongtree.ehospital.service;


import com.hollongtree.ehospital.Repository.TestReportRepository;
import com.hollongtree.ehospital.entity.TestReport;
import com.hollongtree.ehospital.exception.EhospitalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TestReportService {

    @Autowired
    TestReportRepository testReportRepository;

    public TestReport saveTestReport(TestReport testReport) {
      return testReportRepository.save(testReport);
    }



    public TestReport getTestreportForPatient(Integer testReportid)  {

        Optional<TestReport> testreport= testReportRepository.findById(testReportid);

        return testReportRepository.findById(testReportid).get();

    }

    public List<TestReport> getAllTest() {
        return testReportRepository.findAll();
    }

}
