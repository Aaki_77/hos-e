package com.hollongtree.ehospital.service;

import com.hollongtree.ehospital.Repository.PatientRepository;
import com.hollongtree.ehospital.entity.Patient;
import com.hollongtree.ehospital.exception.EhospitalException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PatientService {

    @Autowired
    PatientRepository patientRepository;

     public Patient savePatient(Patient patient) throws EhospitalException{

    	return  patientRepository.save(patient);
 
    }

	public Patient getPatient(Integer patientid) throws EhospitalException {
		
		Patient patient=patientRepository.findById(patientid).orElseThrow(()->new EhospitalException("patientid not found"));
		if(patient==null)
		{
			throw new EhospitalException("patientid not found");
		}
		return patient;
		
	}

	public Page<Patient> getpatientPageble(Pageable pageable) {
		
		Page<Patient> patientpage=patientRepository.findAll(pageable);
		
		return patientpage;
	}

	public Patient getPatientFile(String patientid) {
		//patientRepository.fetchFile(patientid);
		return null;
	}
}
