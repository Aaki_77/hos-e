package com.hollongtree.ehospital.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.AdminLoginDao;
import com.hollongtree.ehospital.Repository.PatientLoginDao;
import com.hollongtree.ehospital.entity.AdminLogin;
import com.hollongtree.ehospital.entity.PatientLogin;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class PatientLoginService {
    @Autowired
    PatientLoginDao patientLoginDao;

    public PatientLogin addPatientLogin(String email, String password) throws EhospitalException {
        PatientLogin login = patientLoginDao.findByemail(email);

        if (login == null) {
            throw new EhospitalException("invalid email");
        } else {
            if (!(login.getPassword().equals(password))) {
                throw new EhospitalException("invalid password");
            }
        }

        return login;
    }
//
//	public PatientLogin updatePatientLogin(PatientLogin login, int loginId) throws EhospitalException {
//
//		PatientLogin log = patientLoginDao.findById(loginId)
//				.orElseThrow(() -> new EhospitalException("loginId not found"));
//		log.setEmail(login.getEmail());
//		log.setName(login.getName());
//		log.setPassword(login.getPassword());
//
//		PatientLogin log1 = patientLoginDao.save(log);
//
//		return log1;
//	}
//
////	@Override
////	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
////		AdminLogin login=adminLoginDao.findByemail(email);
////		if(login==null)
////		{
////			throw new UsernameNotFoundException(email+"not found");
////		}
////		return new User(login.getEmail(),login.getPassword(),new ArrayList<>());
////	}

    public PatientLogin addLogin(PatientLogin patientLogin) {
        PatientLogin pl=patientLoginDao.save(patientLogin);
        return pl;
    }

    public List<PatientLogin> getLogin() {
        List<PatientLogin> pl=patientLoginDao.findAll();
        return pl;
    }

    public PatientLogin addPatientLogin(PatientLogin patientLogin) {

        PatientLogin dl=patientLoginDao.save(patientLogin);
        return dl;

    }

}