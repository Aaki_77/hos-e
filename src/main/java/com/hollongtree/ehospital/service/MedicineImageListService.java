package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.Repository.MedicineImageListDao;
import com.hollongtree.ehospital.entity.MedicineImageList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class MedicineImageListService {
	
	@Autowired
	MedicineImageListDao medicineImageListDao;

	/**
	 * Method for save the data
	 * @param medicineImageList
	 * @return
	 */
	public MedicineImageList addMedicineImageList(MedicineImageList medicineImageList) {
		
		MedicineImageList mil=medicineImageListDao.save(medicineImageList);
		return mil;
	}

	/**
	 * Method for update the medicineImage
	 * @param medicineImageList
	 * @return
	 * @throws EhospitalException 
	 */
	public MedicineImageList updateMedicineImageList(MedicineImageList medicineImageList,int medicineImageId) throws EhospitalException {
		
		MedicineImageList mil=medicineImageListDao.findById(medicineImageId).orElseThrow(()-> new EhospitalException("medicineImageId not found"));

		mil.setImagePath(medicineImageList.getImagePath());
		mil.setMedicineList(medicineImageList.getMedicineList());
		MedicineImageList mil1=medicineImageListDao.save(mil);
		return mil1;
	}

}
