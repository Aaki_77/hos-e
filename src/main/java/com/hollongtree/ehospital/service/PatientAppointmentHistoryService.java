package com.hollongtree.ehospital.service;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.PatientAppointmentHistoryDao;
import com.hollongtree.ehospital.entity.PatientAppointmentHistory;
import com.hollongtree.ehospital.exception.EhospitalException;



@Service
public class PatientAppointmentHistoryService {


	@Autowired
	PatientAppointmentHistoryDao patientAppointmentHistoryDao;

	/**
	 * method for save the patientAppointmentHistory
	 * @param patientAppointmentHistory
	 * @return
	 */
	public PatientAppointmentHistory addPatientAppointmentHistory(PatientAppointmentHistory patientAppointmentHistory)  {

         patientAppointmentHistory.setVideoLink(patientAppointmentHistory.getVideoLink());


		PatientAppointmentHistory pah=patientAppointmentHistoryDao.save(patientAppointmentHistory);

            return pah;

	}

	/**
	 * Method for Update the patientAppointmentHistory
	 * @param patientAppointmentHistory
	 * @return
	 * @throws EhospitalException
	 */
	public PatientAppointmentHistory updatePatientAppointmentHistory(PatientAppointmentHistory patientAppointmentHistory,int patientAppointmentHistoryId) throws EhospitalException,InterruptedException {

		PatientAppointmentHistory pah=patientAppointmentHistoryDao.findById(patientAppointmentHistoryId).orElseThrow(()-> new EhospitalException("patientAppointmentHistoryId not found"));
		pah.setAppointmentEndTime(patientAppointmentHistory.getAppointmentEndTime());
		pah.setAppointmentStartTime(patientAppointmentHistory.getAppointmentStartTime());
		pah.setAppointmentStatus(patientAppointmentHistory.getAppointmentStatus());
		pah.setDoctorDetailsId(patientAppointmentHistory.getDoctorDetailsId());
		pah.setExpireStatus(patientAppointmentHistory.getExpireStatus());
		pah.setPatientsAppointmentDetailsId(patientAppointmentHistory.getPatientsAppointmentDetailsId());
		pah.setVideoLink(patientAppointmentHistory.getVideoLink());
		PatientAppointmentHistory pah1=patientAppointmentHistoryDao.save(pah);
		return pah1;
	}


	public PatientAppointmentHistory getPatientAppointmentHistory(int doctorDetailsId) throws EhospitalException {


		PatientAppointmentHistory pah=patientAppointmentHistoryDao.findBydoctorDetailsId(doctorDetailsId);
		if(pah==null)
		{
			throw new EhospitalException("doctorDetailsId not found");
		}
		return pah;
	}

	public PatientAppointmentHistory getdoctorDetailsByPatientId(String patientId) throws EhospitalException {

		PatientAppointmentHistory pah=patientAppointmentHistoryDao.findByPatientId(patientId);
		if(pah==null)
			throw new EhospitalException("patientId not found");
		return pah;
	}

	public List<PatientAppointmentHistory> getPatientAppointmentHistoryByPatientId(String patientId) throws EhospitalException {
		List<PatientAppointmentHistory> pah=patientAppointmentHistoryDao.getPatientAppointmentHistory(patientId);
		if(pah==null)
			throw new EhospitalException("patientId not found");
		return pah;
	}

	public List<PatientAppointmentHistory> getPatientAppointmentHistory() {

		List<PatientAppointmentHistory> pah = patientAppointmentHistoryDao.findAll();

		return pah;



	}

	public List<PatientAppointmentHistory> getPatientAppointmentHistoryByDate() {
		List<PatientAppointmentHistory> pah= patientAppointmentHistoryDao.filterByDate();
		return pah;
	}

	public PatientAppointmentHistory getappointmenthistoryForPatient(Integer patientAppointmentHistoryId)  {

		Optional<PatientAppointmentHistory> testreport= patientAppointmentHistoryDao.findById(patientAppointmentHistoryId);

		return patientAppointmentHistoryDao.findById(patientAppointmentHistoryId).get();

	}

//	public PatientAppointmentHistory updatePatientAppointmentHistoryLink(PatientAppointmentHistory patientAppointmentHistory,int patientAppointmentHistoryId) throws EhospitalException,InterruptedException {
//
//		PatientAppointmentHistory pah=patientAppointmentHistoryDao.findById(patientAppointmentHistoryId).orElseThrow(()-> new EhospitalException("patientAppointmentHistoryId not found"));
////		pah.setAppointmentEndTime(patientAppointmentHistory.getAppointmentEndTime());
////		pah.setAppointmentStartTime(patientAppointmentHistory.getAppointmentStartTime());
////		pah.setAppointmentStatus(patientAppointmentHistory.getAppointmentStatus());
////		pah.setDoctorDetailsId(patientAppointmentHistory.getDoctorDetailsId());
//		pah.setExpireStatus(patientAppointmentHistory.getExpireStatus());
////		pah.setPatientsAppointmentDetailsId(patientAppointmentHistory.getPatientsAppointmentDetailsId());
//		pah.setVideoLink(patientAppointmentHistory.getVideoLink());
//		PatientAppointmentHistory pah1=patientAppointmentHistoryDao.save(pah);
//		return pah1;
//	}




}





