package com.hollongtree.ehospital.exception;

public class EhospitalException extends Exception
{
	
	private static final long serialVersionUID = 1L;

	public EhospitalException(String message) {
		super(message);
	}
		
}
