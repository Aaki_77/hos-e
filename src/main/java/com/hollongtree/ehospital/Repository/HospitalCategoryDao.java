package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.HospitalCategory;

import java.util.List;
import java.util.Optional;

@Repository
public interface HospitalCategoryDao extends JpaRepository<HospitalCategory, Integer>
{

	@Query(value = "select * from hospital_category\n" +
			"where hospital_category_id=?1",nativeQuery = true)
	List<HospitalCategory> findBycatId(Integer hospitalCategoryId);

	@Query(value = "select * from hospital_category\n" +
			"where hospital_category_name=?1",nativeQuery = true)
	List<HospitalCategory> findByhospitalCategoryName(String hospitalCategoryName);


}
