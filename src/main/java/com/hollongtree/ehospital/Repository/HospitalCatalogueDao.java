package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.HospitalCatalogue;

@Repository
public interface HospitalCatalogueDao extends JpaRepository<HospitalCatalogue, Integer>
{

	HospitalCatalogue findByCatalogueName(String catalogueName);

}
