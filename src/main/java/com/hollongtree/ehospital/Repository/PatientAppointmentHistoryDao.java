package com.hollongtree.ehospital.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.PatientAppointmentHistory;

@Repository
public interface PatientAppointmentHistoryDao extends JpaRepository<PatientAppointmentHistory, Integer> {

	PatientAppointmentHistory findBydoctorDetailsId(int doctorDetailsId);

	@Query(value = "select * from doctor_details as dd "
			+ "join patient_appointment_history as pah on dd.doctor_details_id=pah.doctor_details_id"
			+ " join patient as p on pah.patient_id=p.patient_id where p.patient_id=?1", nativeQuery = true)
	PatientAppointmentHistory findByPatientId(String patientId);

	@Query(value = "select * from patient_appointment_history where patient_id=?1",nativeQuery = true)
	List<PatientAppointmentHistory> getPatientAppointmentHistory(String patientId);

	@Query(value = "SELECT * from patient_appointment_history where appointment_start_time=CURDATE()",nativeQuery = true)
	List<PatientAppointmentHistory> filterByDate();


}
