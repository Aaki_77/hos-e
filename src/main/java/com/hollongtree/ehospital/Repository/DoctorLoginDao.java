package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.DoctorLogin;

@Repository
public interface DoctorLoginDao extends JpaRepository<DoctorLogin, Integer> {

}
