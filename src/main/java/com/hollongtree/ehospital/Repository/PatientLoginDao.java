package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.PatientLogin;

@Repository
public interface PatientLoginDao extends JpaRepository<PatientLogin, Integer>{

    PatientLogin findByemail(String email);


}