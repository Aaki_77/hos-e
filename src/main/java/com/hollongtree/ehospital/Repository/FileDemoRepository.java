package com.hollongtree.ehospital.Repository;

import com.hollongtree.ehospital.entity.Filedemo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDemoRepository extends JpaRepository<Filedemo,Integer> {
}
