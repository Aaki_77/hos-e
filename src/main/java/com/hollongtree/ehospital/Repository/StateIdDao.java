package com.hollongtree.ehospital.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.StateId;

@Repository
public interface StateIdDao extends JpaRepository<StateId, Integer>{

	List<StateId> findByStateName(String stateName);

	StateId save(List<StateId> stateId);

}
