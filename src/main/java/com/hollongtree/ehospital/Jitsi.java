//package com.hollongtree.ehospital;
//
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//
//@SpringBootApplication
//public class Jitsi extends SpringBootServletInitializer {
//
//
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(Jitsi.class);
//    }
//
//    public static void main(String[] args) throws Exception {
//        SpringApplication.run(Jitsi.class, args);
//    }
//
//}
