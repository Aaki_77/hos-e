//package com.hollongtree.ehospital;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
//
//public class ServletInitializer extends SpringBootServletInitializer {
//	private Logger logger = LoggerFactory.getLogger(this.getClass());
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		logger.info("configuration executed ");
//		return application.sources(EhospitalApplication.class);
//	}
//
//}
