package com.hollongtree.ehospital.controller.VideoController;

import java.util.Map;
import java.util.Random;

import com.hollongtree.ehospital.entity.LoginBean;
import com.hollongtree.ehospital.entity.PatientAppointmentHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;



@CrossOrigin("http://localhost:8100")
@Controller
public class WelcomeController {


    // inject via application.properties
    @Value("${welcome.message:test}")
    private String message = "Hello World";

    @GetMapping("/welcome")
    public String welcome(Map<String, Object> model) {


        model.put("message", this.message);
        return "success";
    }


}