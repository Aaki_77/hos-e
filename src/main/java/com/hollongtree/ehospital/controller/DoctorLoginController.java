package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.DoctorLogin;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.DoctorLoginService;

@RestController
public class DoctorLoginController {

	@Autowired
	DoctorLoginService doctorLoginService;

	/**
	 * API for saving the doctor login details, passing the values to the service
	 * class
	 * 
	 * @param doctorLogin
	 * @return
	 */
	@PostMapping(path = "doctorLogin")
	public ResponseEntity<DoctorLogin> addEntryDoctorLogin(@RequestBody DoctorLogin doctorLogin) {
		DoctorLogin dl = doctorLoginService.addDoctorLogin(doctorLogin);
		return ResponseEntity.status(HttpStatus.OK).body(dl);
	}

	/**
	 * API for Updating the doctor login details, passing the value to the service
	 * class
	 * 
	 * @param doctorLogin
	 * @return
	 * @throws EhospitalException 
	 */

	@PutMapping(path = "doctorLoginUpdate/{doctorLoginId}")
	public ResponseEntity<DoctorLogin> saveOrUpdateEntryDoctorLogin(@RequestBody DoctorLogin doctorLogin,@PathVariable("doctorLoginId")int doctorLoginId) throws EhospitalException {
		DoctorLogin dl = doctorLoginService.updateDoctorLogin(doctorLogin,doctorLoginId);
		return ResponseEntity.status(HttpStatus.OK).body(dl);
	}

}
