package com.hollongtree.ehospital.controller;

import com.hollongtree.ehospital.entity.TestReport;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.TestReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:8100")
public class TestReportController {

    @Autowired
    TestReportService testReportService;

    @GetMapping("/testReport")
    public String test7(TestReport testReport){

        return "testReport";
    }

    @PostMapping("/savetestReport")
    @ResponseBody
    public ResponseEntity<TestReport> saveTestReport(@RequestBody TestReport testReport){

        TestReport tr=testReportService.saveTestReport(testReport);

            if(null != testReport.getTestReportid()){

                return new ResponseEntity<TestReport>(tr, HttpStatus.OK);
            }
            return new ResponseEntity<TestReport>(HttpStatus.BAD_REQUEST);


        }




    @GetMapping ("/testreport/{testReportid}")
    public ResponseEntity<TestReport> getTestreportForPatient(@PathVariable Integer testReportid){

        TestReport testReport= testReportService.getTestreportForPatient(testReportid);

        if(testReport != null){

            return new ResponseEntity<TestReport>(testReport, HttpStatus.OK);
        }
        return new ResponseEntity<TestReport>(HttpStatus.BAD_REQUEST);

    }

    @GetMapping("/getTest")
    public ResponseEntity<List<TestReport>> getAllLabtestlist() throws EhospitalException
    {
        List<TestReport> ltl = testReportService.getAllTest();

        return ResponseEntity.status(HttpStatus.OK).body(ltl);
    }

}