package com.hollongtree.ehospital.controller;

//import com.hollongtree.ehospital.Repository.PatientRepository;
//import com.hollongtree.ehospital.entity.Patient;
//import com.hollongtree.ehospital.service.PatientService;
import com.hollongtree.ehospital.Repository.PatientRepository;
import com.hollongtree.ehospital.entity.Patient;
import com.hollongtree.ehospital.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;

@RestController
public class FiledemoController extends NullPointerException {

    @Autowired
    PatientRepository patientRepository;

    @RequestMapping("/Documents")
    public ResponseEntity<Integer> documentGeneration() throws IOException {


        Scanner reader = new Scanner(System.in);
        boolean success = false;


        System.out.println("Enter path of directory to create");
        String dir = reader.nextLine();


        // Creating new directory in Java, if it doesn't exists
        File directory = new File(dir);
        if (directory.exists()) {
            System.out.println("Directory already exists ...");

        } else {
            System.out.println("Directory not exists, creating now");

            success = directory.mkdir();
            if (success) {
                System.out.printf("Successfully created new directory : %s%n", dir);
            } else {
                System.out.printf("Failed to create new directory: %s%n", dir);
            }
        }


        // Creating new file in Java, only if not exists
        System.out.println("Enter Prescription file name to be created ");
        String filename = reader.nextLine();

        System.out.println("Enter patient id");
        int patient = reader.nextInt();


        PatientService ps = new PatientService();
        Optional<Patient> optionalPatient = patientRepository.findById(patient);

        if (optionalPatient.isPresent()) {

            Patient patient1 = optionalPatient.get();

           // Patient patient2 = ps.fetchPatient(dir);

            File f = new File(filename);

            //   System.out.println("No such file exists, creating now");
            success = f.createNewFile();
            if (success) {
                System.out.printf("Successfully created new Prescription file: %s%n", f);
            } else {
                System.out.printf("Failed to create new Prescription file: %s%n", f);
            }


            return new ResponseEntity<Integer>(HttpStatus.OK);
        }
        return null;
    }
}