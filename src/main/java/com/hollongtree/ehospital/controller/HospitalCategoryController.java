package com.hollongtree.ehospital.controller;

import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;
import com.hollongtree.ehospital.service.HospitalSubcategoryDiseaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hollongtree.ehospital.entity.HospitalCategory;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.HospitalCategoryService;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
@CrossOrigin("http://localhost:8100")

public class HospitalCategoryController {

	@Autowired
	HospitalCategoryService hospitalCategoryService;

	@Autowired
	HospitalSubcategoryDiseaseService hospitalSubcategoryDiseaseService;

	/**
	 * Api for adding the values of hospitalcategory entity into the DB for that
	 * passing the value to the service class
	 * 
	 * @param hospitalCategory
	 * @return responseentity httpstatus of the code
	 */
	@PostMapping(path = "/hospitalCategory")
	public ResponseEntity<HospitalCategory> addEntryHospitalCategory(@RequestBody HospitalCategory hospitalCategory) {
		HospitalCategory hc = hospitalCategoryService.addHospitalCategory(hospitalCategory);
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	/**
	 * Api for fetching the data using hospitalcategoryname and handling the
	 * exception if there is any null value
	 * 
	 * @param hospitalCategoryName
	 * @return
	 */
	@GetMapping("hoscat/{hospitalCategoryName}")
	public ResponseEntity<List<HospitalCategory>> getHossubCategory(@PathVariable("hospitalCategoryName") String hospitalCategoryName)
			throws EhospitalException {


		List<HospitalCategory> hc = hospitalCategoryService.getHossubCategory(hospitalCategoryName);
		return ResponseEntity.status(HttpStatus.OK).body(hc);


	}

	/**
	 * Api for updating data passing the param to the service class
	 * 
	 * @param hospitalCategory
	 * @return httpstatus of the code
	 * @throws EhospitalException 
	 */
	@PutMapping(path = "/hospitalCategoryUpdate/{hospitalCategoryId}")
	public ResponseEntity<HospitalCategory> saveOrUpdateHospitalCategory(
			@RequestBody HospitalCategory hospitalCategory,@PathVariable("hospitalCategoryId")int hospitalCategoryId) throws EhospitalException {
		HospitalCategory hc = hospitalCategoryService.updateHospitalCategory(hospitalCategory,hospitalCategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	@GetMapping("/HospitalCategory")
	public ResponseEntity<List<HospitalCategory>> getAllHoscattlist() throws EhospitalException
	{
		List<HospitalCategory> ltl = hospitalCategoryService.getAllHos();

		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}

	@PutMapping("Hoscat/{hospitalCategoryId}")
	public ResponseEntity<HospitalCategory> updateHosclist(@PathVariable("hospitalCategoryId") int hospitalCategoryId,@PathVariable("file") MultipartFile file) throws EhospitalException, IOException, SerialException
			, SQLException
	{
		HospitalCategory ltl = hospitalCategoryService.updateHos(file,hospitalCategoryId);

		return ResponseEntity.status(HttpStatus.OK).body(ltl);
	}


	@GetMapping ("/HospitalCategory/{hospitalCategoryId}")
	public ResponseEntity<List<HospitalCategory>> getCategoryForProducts(@PathVariable Integer hospitalCategoryId)throws EhospitalException{

		List<HospitalCategory> hc = hospitalCategoryService.getCategoryForProducts(hospitalCategoryId);
		return ResponseEntity.status(HttpStatus.OK).body(hc);

	}

	@PostMapping(path = "hospitalDisease/{hospitalCategoryId}")
	public ResponseEntity<HospitalCategory> savehossubcat(
			@RequestBody HospitalCategory hospitalCategory,@PathVariable("hospitalCategoryId") Integer hospitalCategoryId)
	{
		HospitalCategory hsd = hospitalCategoryService.savehossubcat(hospitalCategory);

		return ResponseEntity.status(HttpStatus.OK).body(hsd);
	}




}
