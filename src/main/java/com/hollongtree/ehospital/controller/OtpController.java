package com.hollongtree.ehospital.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Random;

@RestController
public class OtpController {

    @RequestMapping("/otp")
    public ResponseEntity<String> otpGeneration(){

        String numbers = "1234567890";
        Random random = new Random();
        int length=6;

        StringBuffer buffer=new StringBuffer();
//        char[] otp = new char[6];

        for(int i = 0; i< length ; i++) {
            buffer.append(numbers.charAt(random.nextInt(numbers.length())));

        }
        return new ResponseEntity<String>(buffer.toString(), HttpStatus.OK);
    }

}
