package com.hollongtree.ehospital.controller;

import com.hollongtree.ehospital.entity.PrescriptionPage;
import com.hollongtree.ehospital.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PageController {


    @Autowired
    PageService pageService;

    @GetMapping("/page")
    public String test4(PrescriptionPage page){

        return "page";
    }

    @PostMapping("/savePage")
    public ResponseEntity<Integer> savePage(@RequestBody PrescriptionPage page){

       PrescriptionPage pg= pageService.savePage(page);

       if(null != pg.getPageid()){

           return new ResponseEntity<Integer>(page.getPageid(),HttpStatus.OK);
       }
        return new ResponseEntity<Integer>(HttpStatus.BAD_REQUEST);

    }
    
    @GetMapping(path = "/page/{patientid}")
    public ResponseEntity<PrescriptionPage> getPage(@PathVariable("patientid")String patientid){
    	PrescriptionPage prescriptionPage=pageService.getPage(patientid);
		return ResponseEntity.status(HttpStatus.OK).body(prescriptionPage);
    }

}
