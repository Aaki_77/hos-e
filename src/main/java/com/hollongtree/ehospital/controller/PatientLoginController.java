package com.hollongtree.ehospital.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.PatientLogin;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.PatientLoginService;

@RestController
@CrossOrigin("http://localhost:8100")
public class PatientLoginController {

    @Autowired
    private PatientLoginService patientLoginService;

    @PostMapping(path = "log/{email}/{password}")
    public ResponseEntity<PatientLogin> addLogin(@PathVariable("email") String email,
                                                 @PathVariable("password") String password) {

        PatientLogin log;
        try {
            log = patientLoginService.addPatientLogin(email, password);
            return ResponseEntity.status(HttpStatus.OK).body(log);
        } catch (EhospitalException e) {
            log = null;
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(log);
        }

    }

    @PostMapping(path = "/loginPatient")
    public ResponseEntity<PatientLogin> Login(PatientLogin patientLogin) throws EhospitalException {
        PatientLogin log = patientLoginService.addLogin(patientLogin);
        return ResponseEntity.status(HttpStatus.OK).body(log);
    }

    @GetMapping(path = "login/get")
    public ResponseEntity<List<PatientLogin>> greet() {
        List<PatientLogin> log = patientLoginService.getLogin();
        return ResponseEntity.status(HttpStatus.OK).body(log);

    }
//
//	@PutMapping(path = "login/{loginId}")
//	public ResponseEntity<PatientLogin> updateLogin(@RequestBody PatientLogin login,
//			@PathVariable("loginId") int loginId) throws EhospitalException {
//		PatientLogin log = patientLoginService.updatePatientLogin(login, loginId);
//		return ResponseEntity.status(HttpStatus.OK).body(log);
//
//	}
//



    @PostMapping(path = "patientLogin")
    public ResponseEntity<PatientLogin> addEntryDoctorLogin(@RequestBody PatientLogin patientLogin) {
        PatientLogin dl = patientLoginService.addPatientLogin(patientLogin);
        return ResponseEntity.status(HttpStatus.OK).body(dl);
    }

}