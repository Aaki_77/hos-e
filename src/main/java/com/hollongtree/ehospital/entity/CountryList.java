package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "country_list")
public class CountryList {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "country_id")
	private int countryId;
	@Column(name = "country_name")
	private String countryName;
	@Column(name = "country_image_path")
	private String countryImagePath;
	@OneToMany(targetEntity = StateId.class,cascade = CascadeType.ALL)
	@JoinColumn(name = "state_fk_id",referencedColumnName = "country_id")
	private List<StateId> state_id;
	
	public CountryList() {
		
	}

	public int getCountryid() {
		return countryId;
	}

	public void setCountryid(int countryid) {
		this.countryId = countryid;
	}

	public String getCountryname() {
		return countryName;
	}

	public void setCountryname(String countryname) {
		this.countryName = countryname;
	}

	public String getCountryimagepath() {
		return countryImagePath;
	}

	public void setCountryimagepath(String countryimagepath) {
		this.countryImagePath = countryimagepath;
	}

	public List<StateId> getState_id() {
		return state_id;
	}

	public void setState_id(List<StateId> state_id) {
		this.state_id = state_id;
	}
	
	
	

}
