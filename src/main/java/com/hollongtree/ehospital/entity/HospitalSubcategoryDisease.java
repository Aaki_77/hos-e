package com.hollongtree.ehospital.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "hospital_subcategory_disease")
public class HospitalSubcategoryDisease {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hospital_subcategory_id")
	private int hospitalSubcategoryId;
	@Column(name = "hospital_subcategory_name", length = 150)
	private String hospitalSubcategoryName;
	@Column(name = "hospital_subcategory_image_path", length = 150)
	private String hospitalSubcategoryImagePath;


	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name="hospital_category_id")
	private HospitalCategory hospitalcategory;

	@OneToMany(fetch= FetchType.LAZY,cascade=CascadeType.ALL)
	private Set<DoctorDetails> doctorDetails=new HashSet<DoctorDetails>();


	public HospitalSubcategoryDisease() {
	}

	public HospitalSubcategoryDisease(String hospitalSubcategoryName, String hospitalSubcategoryImagePath, HospitalCategory hospitalcategory) {
		this.hospitalSubcategoryName = hospitalSubcategoryName;
		this.hospitalSubcategoryImagePath = hospitalSubcategoryImagePath;
		this.hospitalcategory = hospitalcategory;

	}

	public int getHospitalsubcategoryid() {
		return hospitalSubcategoryId;
	}

	public void setHospitalsubcategoryid(int hospitalsubcategoryid) {
		this.hospitalSubcategoryId = hospitalsubcategoryid;
	}

	public String getHospitalsubcategoryname() {
		return hospitalSubcategoryName;
	}

	public void setHospitalsubcategoryname(String hospitalsubcategoryname) {
		this.hospitalSubcategoryName = hospitalsubcategoryname;
	}

	public String getHospitalsubcategoryimage_path() {
		return hospitalSubcategoryImagePath;
	}

	public void setHospitalsubcategoryimage_path(String hospitalsubcategoryimage_path) {
		this.hospitalSubcategoryImagePath = hospitalsubcategoryimage_path;
	}



	public HospitalCategory getHospitalcategory() {
		return hospitalcategory;
	}

	public void setHospitalcategory(HospitalCategory hospitalcategory) {
		this.hospitalcategory = hospitalcategory;
	}

	public Set<DoctorDetails> getDoctorDetails() {
		return doctorDetails;
	}

	public void setDoctorDetails(Set<DoctorDetails> doctorDetails) {
		this.doctorDetails = doctorDetails;
	}
}