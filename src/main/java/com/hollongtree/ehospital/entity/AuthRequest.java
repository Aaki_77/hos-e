package com.hollongtree.ehospital.entity;


public class AuthRequest {
	
	private String email;
	private String Password;
	
	public AuthRequest() {
	}
	
	
	public AuthRequest(String email, String password) {
		super();
		this.email = email;
		this.Password = password;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return Password;
	}


	public void setPassword(String password) {
		Password = password;
	}
	
	
	
	

}
