package com.hollongtree.ehospital.entity;

import java.util.List;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medical_list")
public class MedicalList {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "medical_id")
	private int medicalId;
	@Column(name = "medical_name")
	private String medicalName;
	@Column(name = "medical_address1")
	private String medicalAddress1;
	@Column(name = "medical_address2")
	private String medicalAddress2;
	@Column(name = "pincode")
	private String pincode;
	@Column(name = "country_id")
	private int countryId;
	@Column(name = "state_id")
	private int stateId;
	@Column(name = "city_id")
	private int cityId;
	@Column(name="location")
	private String location;
	@Column(name = "hospital_id")
	private int hospitalId;
   
	 
	
	public MedicalList() {
	}


	public MedicalList(int medicalId, String medicalName, String medicalAddress1, String medicalAddress2,
			String pincode, int countryId, int stateId, int cityId, String location, int hospitalId,
			List<MedicalList> medicalList) {
		super();
		this.medicalId = medicalId;
		this.medicalName = medicalName;
		this.medicalAddress1 = medicalAddress1;
		this.medicalAddress2 = medicalAddress2;
		this.pincode = pincode;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.location = location;
		this.hospitalId = hospitalId;
	}


	public int getMedicalId() {
		return medicalId;
	}


	public void setMedicalId(int medicalId) {
		this.medicalId = medicalId;
	}


	public String getMedicalName() {
		return medicalName;
	}


	public void setMedicalName(String medicalName) {
		this.medicalName = medicalName;
	}


	public String getMedicalAddress1() {
		return medicalAddress1;
	}


	public void setMedicalAddress1(String medicalAddress1) {
		this.medicalAddress1 = medicalAddress1;
	}


	public String getMedicalAddress2() {
		return medicalAddress2;
	}


	public void setMedicalAddress2(String medicalAddress2) {
		this.medicalAddress2 = medicalAddress2;
	}


	public String getPincode() {

		return pincode;
	}


	public void setPincode(String pincode) {
		this.pincode = pincode;
	}


	public int getCountryId() {
		return countryId;
	}


	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}


	public int getStateId() {
		return stateId;
	}


	public void setStateId(int stateId) {
		this.stateId = stateId;
	}


	public int getCityId() {
		return cityId;
	}


	public void setCityId(int cityId) {
		this.cityId = cityId;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public int getHospitalId() {
		return hospitalId;
	}


	public void setHospitalId(int hospitalId) {
		this.hospitalId = hospitalId;
	}



	
	

}
