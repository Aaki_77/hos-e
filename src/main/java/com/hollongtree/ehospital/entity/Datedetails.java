//package com.hollongtree.ehospital.entity;
//
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//
//import javax.persistence.*;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Set;
//
//@Entity
//@Table(name = "Date_details")
//public class Datedetails {
//
//
//    private Long dateId;
//
//    @JsonFormat
//            (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
//    private Date date;
//
//    private Set<Patient> patient = new HashSet<Patient>(0);
//
//    private Set<DoctorDetails> doctorDetails = new HashSet<>(0);
//
//
//    @Id
//    @Column(name = "DateID")
//    @GeneratedValue
//    public Long getDateId() {
//        return dateId;
//    }
//
//
//    public void setDateId(Long dateId) {
//        this.dateId = dateId;
//    }
//
//    @Column(name = "date")
//    public Date getDate() {
//        return date;
//    }
//
//    public void setDate(Date date) {
//        this.date = date;
//    }
//
//    public void setPatient(Set<Patient> patient) {
//        this.patient = patient;
//    }
//
//
//    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "date", cascade = {
//            CascadeType.MERGE, CascadeType.PERSIST
//    })
//    public Set<Patient> getPatient() {
//        return patient;
//    }
//
//
////    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "date", cascade = {
////            CascadeType.MERGE, CascadeType.PERSIST
////    })
////    public Set<DoctorDetails> getDoctorDetails() {
////
////        return doctorDetails;
////    }
////
////    public void setDoctorDetails(Set<DoctorDetails> doctorDetails) {
////        this.doctorDetails = doctorDetails;
////    }
////}
//}