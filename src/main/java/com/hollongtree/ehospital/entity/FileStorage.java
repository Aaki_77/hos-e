//package com.hollongtree.ehospital.entity;
//
//import javax.persistence.*;
//import java.util.HashSet;
//import java.util.Set;
//
//@Entity
//@Table(name = "documents")
//public class FileStorage {
//
//    private int fileId;
//
//    private String pagepath;
//
//    private Set<PrescriptionList> prescriptionset = new HashSet<PrescriptionList>(0);
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "files_id", unique = true, nullable = false)
//    public int getFileId() {
//        return fileId;
//    }
//
//    public void setFileId(int fileId) {
//        this.fileId = fileId;
//    }
//
//    @Column(name = "page_path", nullable = false)
//    public String getPagepath() {
//        return pagepath;
//    }
//
//    public void setPagepath(String pagepath) {
//        this.pagepath = pagepath;
//    }
//
//
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fileStorage")
//    public Set<PrescriptionList> getPrescriptionset() {
//        return prescriptionset;
//    }
//
//    public void setPrescriptionset(Set<PrescriptionList> prescriptionset) {
//        this.prescriptionset = prescriptionset;
//    }
////}
//}
