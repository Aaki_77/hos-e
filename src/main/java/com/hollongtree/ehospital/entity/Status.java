package com.hollongtree.ehospital.entity;

import javax.persistence.*;

@Entity
@Table(name = "Status")
public class Status {

    private int statusId;

    private String ok;

    private String sucess;

    private String denied;

    @Id
    @Column(name = "StatusID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    @Column(name="Ok")
    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    @Column(name="Successful")
    public String getSucess() {
        return sucess;
    }

    public void setSucess(String sucess) {
        this.sucess = sucess;
    }

    @Column(name="Cancel")
    public String getDenied() {
        return denied;
    }

    public void setDenied(String denied) {
        this.denied = denied;
    }
}
