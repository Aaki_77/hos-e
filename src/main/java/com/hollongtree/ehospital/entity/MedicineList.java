package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "medicine_list")
public class MedicineList {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "medicine_id")
	private int medicineId;
	@Column(name = "medicine_name")
	private String medicineName;
	@Column(name = "medicine_description")
	private String medicineDescription;
	@Column(name = "udf1")
	private String udf1;
	@Column(name = "udf2")
	private String udf2;
	@Column(name = "udf3")
	private String udf3;
	@Column(name = "udf4")
	private String udf4;
	@Column(name = "udf5")
	private String udf5;
	@Column(name = "udf6")
	private String udf6;
	@Column(name = "udf7")
	private String udf7;
	@ManyToMany
	private List<MedicalList> medicalList;
	
	
	public MedicineList() {
	}

	public MedicineList(int medicineId, String medicineName, String medicineDescription, String udf1, String udf2,
			String udf3, String udf4, String udf5, String udf6, String udf7, List<MedicalList> medicalList,
			List<MedicineImageList> medicineImageLists) {
		super();
		this.medicineId = medicineId;
		this.medicineName = medicineName;
		this.medicineDescription = medicineDescription;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.udf6 = udf6;
		this.udf7 = udf7;
		this.medicalList = medicalList;
	}

	public int getMedicineId() {
		return medicineId;
	}

	public void setMedicineId(int medicineId) {
		this.medicineId = medicineId;
	}

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public String getMedicineDescription() {
		return medicineDescription;
	}

	public void setMedicineDescription(String medicineDescription) {
		this.medicineDescription = medicineDescription;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getUdf6() {
		return udf6;
	}

	public void setUdf6(String udf6) {
		this.udf6 = udf6;
	}

	public String getUdf7() {
		return udf7;
	}

	public void setUdf7(String udf7) {
		this.udf7 = udf7;
	}

	public List<MedicalList> getMedicalList() {
		return medicalList;
	}

	public void setMedicalList(List<MedicalList> medicalList) {
		this.medicalList = medicalList;
	}

	
	
	
	
}
	  
	 
	