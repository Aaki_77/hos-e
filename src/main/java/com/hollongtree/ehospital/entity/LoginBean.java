package com.hollongtree.ehospital.entity;

import javax.persistence.*;

@Entity
@Table(name="LoginBean")
public class LoginBean {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="Username")
    private String userName;

     @Column(name="password")
    private String password;

    @Column(name="Link")
     private String Link;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}