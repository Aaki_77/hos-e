package com.hollongtree.ehospital.entity;

import com.hollongtree.ehospital.exception.EhospitalException;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;
import java.util.Random;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "patient_appointment_history")
public class PatientAppointmentHistory  {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "patient_appointment_history_id")
	private int patientAppointmentHistoryId;
	@Column(name = "appointment_start_time")
	private Date appointmentStartTime;
	@Column(name = "appointment_end_time")
	private Date appointmentEndTime;
	@Column(name = "expireStatus")
	private String expireStatus;
	@ManyToMany
	@Column(name = "patients_appointment_details_id")
	@ElementCollection(targetClass = PatientPaymentDetails.class)
	private Set<PatientPaymentDetails> patientsAppointmentDetailsId;
	@Column(name = "appointment_is_done")
	private boolean appointmentIsDone;
	@Column(name = "doctor_details_id")
	private int doctorDetailsId;
	@Column(name = "video_link")
	private String videoLink;
	@Column(name = "appointment_status")
	private String appointmentStatus;
	@Column(name = "patient_id")
	private String patientId;

	public PatientAppointmentHistory() {
	}

	public PatientAppointmentHistory(int patientAppointmentHistoryId, Date appointmentStartTime,
									 Date appointmentEndTime, String expireStatus, Set<PatientPaymentDetails> patientsAppointmentDetailsId, boolean appointmentIsDone,
									 int doctorDetailsId, String videoLink, String appointmentStatus) {
		super();
		this.patientAppointmentHistoryId = patientAppointmentHistoryId;
		this.appointmentStartTime = appointmentStartTime;
		this.appointmentEndTime = appointmentEndTime;
		this.expireStatus = expireStatus;
		this.patientsAppointmentDetailsId = patientsAppointmentDetailsId;
		this.appointmentIsDone = appointmentIsDone;
		this.doctorDetailsId = doctorDetailsId;
		this.videoLink = videoLink;
		this.appointmentStatus = appointmentStatus;
	}

	public int getPatientAppointmentHistoryId() {
		return patientAppointmentHistoryId;
	}

	public void setPatientAppointmentHistoryId(int patientAppointmentHistoryId) {
		this.patientAppointmentHistoryId = patientAppointmentHistoryId;
	}

	public Date getAppointmentStartTime() {
		return appointmentStartTime;
	}

	public void setAppointmentStartTime(Date appointmentStartTime) {
		this.appointmentStartTime = appointmentStartTime;
	}

	public Date getAppointmentEndTime() {
		return appointmentEndTime;
	}

	public void setAppointmentEndTime(Date appointmentEndTime) {
		this.appointmentEndTime = appointmentEndTime;
	}

	public String getExpireStatus() {
		return expireStatus;
	}

	public void setExpireStatus(String expireStatus) {
		this.expireStatus = expireStatus;
	}

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "patient_appointment_history")
	public Set<PatientPaymentDetails> getPatientsAppointmentDetailsId() {
		return patientsAppointmentDetailsId;
	}

	public void setPatientsAppointmentDetailsId(Set<PatientPaymentDetails> patientsAppointmentDetailsId) {
		this.patientsAppointmentDetailsId = patientsAppointmentDetailsId;
	}

	public boolean isAppointmentIsDone() {
		return appointmentIsDone;
	}

	public void setAppointmentIsDone(boolean appointmentIsDone) {
		this.appointmentIsDone = appointmentIsDone;
	}

	public int getDoctorDetailsId() {
		return doctorDetailsId;
	}

	public void setDoctorDetailsId(int doctorDetailsId) {
		this.doctorDetailsId = doctorDetailsId;
	}

	public String getVideoLink()  {

		return videoLink;

	}

	public synchronized void setVideoLink(String videoLink) {


		String words = "abcdefghijklmnopqrstuvwxyz1234567890";
		Random random = new Random();
		int length = 8;

		StringBuffer buffer = new StringBuffer();
		char[] otp = new char[8];

		for (int i = 0; i < length; i++) {
			buffer.append(words.charAt(random.nextInt(words.length())));

		}
		 videoLink = buffer.toString();

		this.videoLink = videoLink;
	}

	public String getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public String getPatientid() {
		return patientId;
	}

	public void setPatientid(String patientid) {
		this.patientId = patientid;
	}


}
