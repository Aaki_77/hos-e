package com.hollongtree.ehospital.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "hospital_catalogue")
public class HospitalCatalogue {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hospital_catalogue_id")
	private int hospitalCatalogueId;
	@Column(name = "catalogue_name",length = 100)
	private String catalogueName;
	@Column(name = "catalogue_image_path",length = 100)
	private String catalogueImagePath;

	@OneToMany(fetch= FetchType.LAZY,cascade = CascadeType.ALL)
	private Set<HospitalCategory> hospitalcategory=new HashSet<HospitalCategory>();


	@OneToMany(fetch= FetchType.LAZY,cascade = CascadeType.ALL)
	private Set<DoctorDetails> doctorDetails=new HashSet<DoctorDetails>();


	public HospitalCatalogue() {
	}

	public HospitalCatalogue(int hospitalCatalogueId, String catalogueName, String catalogueImagePath, Set<HospitalCategory> hospitalcategory) {
		this.hospitalCatalogueId = hospitalCatalogueId;
		this.catalogueName = catalogueName;
		this.catalogueImagePath = catalogueImagePath;
		this.hospitalcategory = hospitalcategory;
	}

	public int getHospitalcatalogueid() {
		return hospitalCatalogueId;
	}

	public void setHospitalcatalogueid(int hospitalcatalogueid) {
		this.hospitalCatalogueId = hospitalcatalogueid;
	}

	public String getCataloguename() {
		return catalogueName;
	}

	public void setCataloguename(String cataloguename) {
		this.catalogueName = cataloguename;
	}

	public String getCatalogueimagepath() {
		return catalogueImagePath;
	}

	public void setCatalogueimagepath(String catalogueimagepath) {
		this.catalogueImagePath = catalogueimagepath;
	}

	public Set<HospitalCategory> getHospitalcategory() {
		return hospitalcategory;
	}

	public void setHospitalcategory(Set<HospitalCategory> hospitalcategory) {
		this.hospitalcategory = hospitalcategory;
	}
}
	
	